
latest_messages = null;
function getRecentMails() {
    (function ($) {
        /*  This function collects the last 25 mails 
        */
        $.ajax({
            type: "GET",
            url: "/mails/",
            success: function (messages) {
                latest_messages = messages;
                html = `<table id='latest-mails' class='table table-hover'>
                    <thead>
                    <tr>
                        <th scope='col'>Sender</th>
                        <th scope='col'>Subject</th>
                        <th scope='col'>Time</th>
                    </tr>
                    </thead>
                    <tbody>`;
                for (let message of latest_messages) {
                    html += `<tr onclick="readMessage('${message.ID}')">
                        <td>${message.from}</td>
                        <td>${ message.subject}</td>
                        <td>${message.date}</td>
                    </tr>`;
                }
                html += `</tbody></table>`;
                document.querySelector("#mails-list").innerHTML = html;
            }
        });
    })(jQuery)
}

function toggleContent(c){
    document.querySelector("#mails-list").style.display = "none";
    document.querySelector("#message-body").style.display = "none";
    document.querySelector("#composer").style.display = "none";
    document.querySelector(`#${c}`).style.display = "block";
}

function readMessage(msg_id) {
    (function ($) {
        document.querySelector("#mails-list").style.display = "none";
        document.querySelector("#message-body").style.display = "block";
        let message = latest_messages.find(function(element) {return element.ID == msg_id;});
        document.querySelector("#message-body").innerHTML = message.body;
    })(jQuery)
}

function sendMail(){
    btnSend = document.querySelector('#btn-send');
    btnCancel = document.querySelector('#btn-cancel');
    btnSend.innerHTML = `<i class='fas fa-spinner fa-w-16 fa-spin fa-xs'>`;
    btnSend.style.width = "60px";
    btnCancel.disabled = true;
    var data = new FormData(document.querySelector("#frm-compose"));
    $.ajax({
        type:"POST",
        processData: false,
        contentType: false,
        cache: false,
        data:data,
        url:"/mails/send",
        success:function(msg){
            toggleContent("mails-list");
            document.querySelector("#mails-list").insertAdjacentHTML("afterbegin", `
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    ${msg}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            `);
        },
        error:function(){
            document.querySelector("#composer").insertAdjacentHTML("afterbegin", `
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    An error occured. The message could not be sent!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            `);
        },
        complete:function(){
            btnSend.innerHTML = `send`;
            btnCancel.disabled = false;
            // toggleContent("mails-list");
        }
    });
}

(function ($) {
    $(window).on('load', function () {
        toggleContent("mails-list")
        getRecentMails();
    });
})(jQuery)
