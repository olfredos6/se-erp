from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import Template, Context
import imaplib, email, string
from dateutil import parser
from django.http import JsonResponse, HttpResponseServerError
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.mail import send_mail


recent_mails_table = Template("<table id='latest-mails' class='table table-hover'>\
  <thead>\
    <tr>\
      <th scope='col'>Sender</th>\
      <th scope='col'>Subject</th>\
      <th scope='col'>Time</th>\
    </tr>\
  </thead>\
  <tbody>\
    {% for mail in mails %}\
    <tr>\
      <td data-toggle='tooltip' data-placement='right' title='{{ mail.from }}}'>{{ mail.from|safe }}</td>\
      <td>{{ mail.subject|truncatechars:70 }}</td>\
      <td>{{ mail.date }}</td>\
    </tr>\
    {% endfor %}\
  </tbody>\
</table>")

letters = string.ascii_uppercase + string.ascii_lowercase + "^"
numbers = string.digits
def recent(request):
  imap_host = 'imap.gmail.com'
  imap_user = '214150267@edu.vut.ac.za'
  imap_pass = 'theobromagema'

  # connect to host using SSL 
  with imaplib.IMAP4_SSL(imap_host) as imap:
    ## login to server
    imap.login(imap_user, imap_pass)
    resp, data = imap.select("INBOX")
    r, data = imap.search(None, "ALL")
    ids = data[0].split()
    ids = [ id.decode() for id in ids[-20:]]
    mails = []
    for id in ids:
      r, data = imap.fetch(id, "RFC822")
      parsed_email = email.message_from_string(data[0][1].decode())
    
      # We replace the datetime format
      date = parser.parse(parsed_email["Date"]).strftime("%a %d, %H:%M")
      mail_from = parsed_email["From"]
      subject = parsed_email["Subject"]
      body = None
      maintype = parsed_email.get_content_maintype()

      if maintype == 'multipart':
        for part in parsed_email.get_payload():
          if part.get_content_maintype() == 'text':
            body = part.get_payload(decode=True).decode("utf-8")
      elif maintype == 'text':
          body = parsed_email.get_payload()
      # We save
      mails.append({"ID": parsed_email["message-id"], "from":mail_from, "subject":subject, "date":date, "body":body})
  return JsonResponse(mails, safe=False)


def send(request):
  if request.method == "POST":
    subject = request.POST.get("subject", "")
    recipient = request.POST.get("email", "")
    body = request.POST.get("body", "")
    try:
      send_mail(
              subject,
              '{}'.format(body),
              '214150267@vut.edu.ac.za',
              [recipient],
              fail_silently=False,
              )
      return HttpResponse("Message successfully sent to {}".format(recipient))
    except Exception as e:
      print(e)
  return HttpResponseServerError()