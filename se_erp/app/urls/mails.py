from django.urls import path
from app.views import mails

urlpatterns = [
    path('', mails.recent, name='recent_mails'),
    path('send', mails.send, name='send_mail'),
]