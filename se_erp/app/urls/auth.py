from django.urls import path
from app.views import auth

urlpatterns = [
    path('', auth.login, name='login'),
]